package com.myProject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OnlineStoreWithAdministrationPanelApplication {

	public static void main(String[] args) {
		SpringApplication.run(OnlineStoreWithAdministrationPanelApplication.class, args);
	}

}
