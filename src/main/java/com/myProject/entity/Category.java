package com.myProject.entity;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Category
{
    @Id
    private int categoryId;
    private String categoryName;
    private String categoryType;

    public Category() {
    }
public Category(String categoryName, String categoryType){
    this.categoryName = categoryName;
    this.categoryType = categoryType;
}


}
